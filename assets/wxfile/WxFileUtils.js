var fs = null

var allfiles = [];
var allfolds = [];
var progress = {}
var currentProgress = 0;
function dispatchProgress(t,p){
   if(t == 0){
     currentProgress = Math.max(currentProgress, p *0.5)
     progress.pro && progress.pro(currentProgress);
   } else if(t == 1) {
     currentProgress = p*0.5+0.5
     progress.pro && progress.pro(currentProgress);
   } else {
     progress.comp && progress.comp(allfiles, allfolds);
   }
}

module.exports = {
  clearFile:function(path,pro,comp){
    fs = wx.getFileSystemManager();
    allfiles = [];
    allfolds = [];
    currentProgress = 0;

    progress.pro = pro;
    progress.comp = comp;
    var self = this;
    this.readFiles(path,function(){
      dispatchProgress(1,0)
      self.removeFiles(allfiles,function(){
        self.removeFolds()
      })
    },0)
  },
  removeFiles:function(files,scb){
    var acyn = new CCAsync()
    var func = [];
    var deleteIdx = 0;
    var len = allfiles.length + allfolds.length
    for (var i = files.length - 1; i >= 0; i--) {
      
      var fold = files[i];
      func.push(function(cb1){
        var msg = fs.unlink({
          filePath: this,
          success: function (res) {
            deleteIdx+=1
            dispatchProgress(1, deleteIdx / len)
            cb1(null,1)
          }.bind(this),
          fail: function (e) {
            deleteIdx += 1
            console.error('删除文件出错了:', this);
            dispatchProgress(1, deleteIdx / len)
            cb1(null)
          }.bind(this),
        })
      }.bind(fold))
    }
    acyn.parallel(func, function (err, res) {
      scb()
    })
  },
  removeFolds:function(){
    var deleteIdx = 0;
    var len = allfiles.length + allfolds.length
    var start = allfiles.length
      var func = [];
      for(var i=allfolds.length-1; i>=0; i--){
        var fold = allfolds[i];
        func.push(function(f,cb){
          var msg = fs.rmdir({
            dirPath: f,
            success: function () {
              deleteIdx+=1
                dispatchProgress(1, (deleteIdx+start) / len)
              cb(null,1)
            }.bind(fold),
            fail: function (e) {
              deleteIdx += 1
              console.error('删除文件夹出错了:', this);
              dispatchProgress(1, (deleteIdx + start) / len)
              cb(e)
            }.bind(fold)
          })
        }.bind(null, fold))
      }
      var acyn = new CCAsync()
      acyn.parallel(func, function (err, res) {
        dispatchProgress(2);
      })
  },

  readFiles:function(dir,cb,order){
    var self = this
    fs.readdir({
      dirPath: dir,
      success: function (res) {
        var ac = new CCAsync();
        var filefuncs = [];
        var hasfold = false
        for (var i = 0; i < res.files.length; i++) {
          var file = dir + '/' + res.files[i];
          var stat = fs.statSync(file)
          if (stat.isFile()) {
            allfiles.push(file);
            
          } else {
            allfolds.push(file);
            filefuncs.push(function(cb1){
              self.readFiles(this, cb1, order + 1)
            }.bind(file))
          }
          dispatchProgress(0, (allfiles.length + allfolds.length) / 1000);
        }
        if (filefuncs.length >= 0){
          ac.parallel(filefuncs,function(){
            cb(null, order);
          })
        } else {
          cb(null, order)
        }
      },
      fail:function(res){
        cb(null, order)
      }
    })
  }
}