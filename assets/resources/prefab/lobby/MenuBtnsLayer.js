
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    onLoad(){
        this._super();
        this.leftBtns = [];
        this.rightBtns = []
        this.leftBtns.push(this.node.getChildByName('rank'));
        this.leftBtns.push(this.node.getChildByName('friend'));
        this.leftBtns.push(this.node.getChildByName('guild'));
        this.leftBtns.push(this.node.getChildByName('bottle'));
        this.leftBtns.push(this.node.getChildByName('dayreward'));

        this.rightBtns.push(this.node.getChildByName('pack'));
        this.rightBtns.push(this.node.getChildByName('chat'));
        this.rightBtns.push(this.node.getChildByName('tycoon'));
        this.rightBtns.push(this.node.getChildByName('hunt'));
        this.rightBtns.push(this.node.getChildByName('fish'));
        var name = this.name
        for(var i=0; i<this.leftBtns.length; i++){
            var btn = this.leftBtns[i];
            global.Common.addButtonHandler(this,btn,'onBtnClick',i);
        }
        for(var i=0; i<this.rightBtns.length; i++){
            var btn = this.rightBtns[i];
            global.Common.addButtonHandler(this,btn,'onBtnClick',10+i);
        }
        global.Event.on('updateButtonHint',this.updateButtonHint.bind(this),this);
        this.updateButtonHint();
    },
    onDestroy(){
        global.Event.off('updateButtonHint');
    },
    updateButtonHint(){
        var self = this;
        this.httpGet('/game/gameUser','easyInfo',{},function(err,res){
            // cc.log('easeInfo',res);
            
            self.leftBtns[1].getComponent('CM-TagButton').setOptions({tag:res.friend>0});
            self.leftBtns[2].getComponent('CM-TagButton').setOptions({tag:res.guild.applyCount+res.guild.finishTask>0});
            self.leftBtns[3].getComponent('CM-TagButton').setOptions({tag:res.bottle.discardCount+res.bottle.pickupCount>0});

            var day = new Date();
            var ld = new Date(global.Data.dayPowerTime);
            var get = false;
            if((day.getHours() >= 12 && day.getHours() < 14) && (ld.getHours() < 12 || day.getHours() >= 14)){
                get = true;
            } else if((day.getHours() >= 18 && day.getHours() < 20) && (ld.getHours() < 18 || day.getHours() >= 20)){
                get = true;
            }
            get = get || global.Data.freePowerCount>0 || global.Data.day>0
            self.leftBtns[4].getComponent('CM-TagButton').setOptions({tag:get});
            self.rightBtns[1].getComponent('CM-TagButton').setOptions({tag:res.chat.newMsg>0});
            Object.assign(global.Data,{driftBottle:res.bottle})
       })

        // var bottle = global.Data.driftBottle.discardCount+global.Data.driftBottle.pickupCount
        // this.leftBtns[3].getComponent('CM-TagButton').setOptions({tag:bottle>0});
    },

    loadLayer(arr,layerId,js,options){
        var _js = options != undefined ? js : arr[layerId].substring(arr[layerId].lastIndexOf('/')+1)
        var _opt = options == undefined ? js : options
        global.Loader.loadResourcesArray(arr,function(err,res){
            var chat = global.UIMgr.pushUI(arr[layerId]);
            if(_opt){
                chat.getComponent(_js).setOptions(_opt);
            }
        })
    },

    onBtnClick(event,data){
        if(data == '0'){
            // global.UIMgr.loadLayer(['UI/rank/RankLayer'],0,0)
            global.UIMgr.loadLayer('Rank',0);
        } else if(data == '1'){ 
            global.UIMgr.loadLayer('Chat',0);
        } else if(data == '2'){
            if(!!global.Data.guildId){
                
                global.UIMgr.loadLayer('Guild',2,'GuildMain',1)
            } else {
                global.UIMgr.loadLayer('Guild',0);
            }
        } else if(data =='3'){
            global.UIMgr.loadLayer('Bottle',0)
        } else if(data == '4'){
            global.UIMgr.loadLayer('DayReward',0)
        } else if(data =='10'){
            global.UIMgr.loadLayer('Pack',0)
        } else if(data == '11'){
            global.UIMgr.loadLayer('Chat','UI/chat/ChatLayer',2);
        } else if(data == '13'){
            global.UIMgr.loadLayer('Extra-Relic',0)
        }
        else if(data == '14'){
        }
    },

    onEnterWindowBegin(){
        for(var i=0; i<this.leftBtns.length; i++){
            var btn = this.leftBtns[i];
            btn.x = -430;
            btn.runAction(cc.sequence(cc.delayTime(0.1*(i+1)),cc.moveBy(0.2,cc.v2(130,0))));
        }
        for(var i=0; i<this.rightBtns.length; i++){
            var btn = this.rightBtns[i];
            btn.x = 412;
            btn.runAction(cc.sequence(cc.delayTime(0.1*(i+1)),cc.moveBy(0.2,cc.v2(-100,0))));
        }
        
        // this.updateButtonHint();
    },

    onExitWindowBegin(){
        for(var i=0; i<this.leftBtns.length; i++){
            var btn = this.leftBtns[i];
            btn.runAction(cc.sequence(cc.delayTime(0.05*(i+1)),cc.moveBy(0.2,cc.v2(-130,0))));
        }
        for(var i=0; i<this.rightBtns.length; i++){
            var btn = this.rightBtns[i];
            btn.runAction(cc.sequence(cc.delayTime(0.05*(i+1)),cc.moveBy(0.2,cc.v2(100,0))));
        }
    },
    onEnterWindowEnd(){
        this._super();
        this.updateButtonHint();
    }
});
