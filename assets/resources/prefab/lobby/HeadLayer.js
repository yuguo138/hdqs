
cc.Class({
    extends:  require('UI-PanelWindow'),

    properties: {
        level : cc.Label,
        wood : require('JumpText'),
        diamond : require('JumpText')
    },

    onLoad(){
        this._super();
        global.Event.on('updateResource',this.updateResource.bind(this));
    },

    onDestroy(){
        global.Event.off('updateResource');
    },

    updateResource(){
        this.level.string = global.Data.store.civilizationLevel;
        this.wood.setText(global.Data.store.wood);
        this.diamond.setText(global.Data.store.mineral);
    },

    onEnterWindowBegin(){
        cc.log('head panel onEnterWindowBegin')
        this.node.y = cc.winSize.height / 2 + 100
        this.node.runAction(cc.moveTo(0.5,cc.v2(this.node.x,cc.winSize.height / 2 -60)));

        this.updateResource();
    }

});
