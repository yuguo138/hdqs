const UI = require('wy-UI');
cc.Class({
    extends: require('wy-Component'),

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    onLoad () {
        this.js_icon = cc.find('headInfo/icon',this.node).getComponent('ImageLoader');
        this.lb_name = cc.find('headInfo/name',this.node).getComponent(cc.Label);
        this.lb_level = cc.find('headInfo/level',this.node).getComponent(cc.Label);
        this.js_sex = cc.find('headInfo/sex',this.node).getComponent('UI-Toggle');
    },

    start () {

    },

    onBtnFriend(event,data){
        var self = this;
        this.httpGet('/game/gameuser','sociality.getFriendsList',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                node.getComponent('FramFriends').setOptions(res.friends,'好友列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },

    onBtnEnemy(event,data){
        var self = this;
        this.httpGet('/game/gameuser','sociality.getEnemyList',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                node.getComponent('FramFriends').setOptions(res.enemy,'仇人列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },
    onBtnGotoFriend(_id){
        this.httpGet('/game/gameuser','island.vegetableField.stealer',{_id:_id}, (err,res)=>{
            if(!err){
                this.setOptions(res);
            }
        })
    },

    onBtnSelect(event,data){
        cc.log('data:',data);

        this.httpGet('/game/gameuser','island.vegetableField.stealIsland',{_id:this.option._id,part:data}, function(err,res){
            cc.log('res:',res);
            var msg = '恭喜阁下偸取到 ';
            if(data == 'water'){
                global.Data.store.water += res.value;
                msg+='水'
            } else if(data == 'fire'){
                global.Data.store.fire += res.value;
                msg+='火'
            } else if(data == 'lumberyard'){
                global.Data.store.wood += res.value;
                msg+='木材' 
            } else if(data == 'quarry'){
                global.Data.store.mineral += res.value;
                msg+='矿石'
            }
            msg+=' '+res.value
            //okButtonText
            UI.Alert({title:'偷取成功',message:msg,msg,okButtonCallback:function(){
                global.GameLoop.mutualToMainLayer();
            }})
            // global.GameLoop.toBrokeLayer().getComponent('Broke').setOptions(res);
            // global.GameLoop.mutualToMainLayer();
        })
        
        //stealIsland
        // global.GameLoop.mutualToMainLayer();
    },

    updateUserInfo(){
        this.js_icon.setOptions({url:this.option.headImgUrl})
        this.lb_name.string = this.option.nickname;
        this.lb_level.string = this.option.civilizationLevel;
        this.js_sex.setToggle(this.option.sex);
        var daoyu = this.node.getChildByName('daoyu');
        cc.find('daoyu/fram',this.node).getComponent(cc.Button).interactable = false
        cc.find('daoyu/burrow',this.node).getComponent(cc.Button).interactable = this.option.island.burrow.isBuild
        cc.find('daoyu/wood',this.node).getComponent(cc.Button).interactable = this.option.island.lumberyard
        cc.find('daoyu/fire',this.node).getComponent(cc.Button).interactable = this.option.island.fire.isBuild
        cc.find('daoyu/quarry',this.node).getComponent(cc.Button).interactable = this.option.island.quarry

        if(!this.option.island.burrow.isBuild && !this.option.island.lumberyard && !this.option.island.fire.isBuild && !this.option.island.quarry)
            global.UI.Alert({title:'提示',message:'没有可偷取位置',okButtonCallback:function(){
                global.GameLoop.mutualToMainLayer();
            }}
        )
        // cc.find('daoyu/mask',this.node).active = this.option.island.bur.isBuild;
    },

    setOptions(option){
        this.option = option;
        this.updateUserInfo();
    }

});
