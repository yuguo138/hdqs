
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    start () {
        // var array = {power:0,water:1,corn:2,fire:3,mineral:4,potato:5,soy:7,wood:9,sorghum:8};
        var array = {7:'soy',5:'potato',2:'corn',8:'sorghum'}
        for(var key in global.DataConfig.seeds){
            var seed = global.DataConfig.seeds[key];
            var item = global.DataConfig.storage[seed.id];
            var node = cc.find('views/seed'+key,this.node);
            var t = array[seed.id]
            node.getComponent('Prop').setId(seed.id,global.Data.store[t]);
            node.getChildByName('desc').getComponent(cc.Label).string = '成熟时间:'+Math.floor(seed.harvestTime/(60*1000))+'分钟';

            global.Common.addButtonHandler(this,node.getChildByName('btn'),'onBtnSelect',seed.id);
        }
    },

    onBtnSelect(event,d){
        cc.log('d:',d)
        this.selectCB && this.selectCB(d);
    },
    setOptions(cb){
        this.selectCB = cb;
    }

    // update (dt) {},
});
