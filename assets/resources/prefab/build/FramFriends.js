
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        FriendItem :cc.Prefab,
        content :cc.Node,
    },

    setOptions(option,title,cb,btnText){
        this.selectCB = cb
        this.option = option;
        cc.find('views/title',this.node).getComponent(cc.Label).string = title
        cc.log('setOptions:',option);
        for(var i=0; i<option.length; i++){
            var friend = option[i];
            var item = cc.instantiate(this.FriendItem);
            this.content.addChild(item);
            item.getComponent('UserInfo').setOptions(friend)
            // item.getChildByName('icon').getComponent('ImageLoader').setOptions({url:friend.headImgUrl})
            // item.getChildByName('name').getComponent(cc.Label).string = friend.nickname;
            // item.getChildByName('nan').active = friend.sex ==  '0'
            // item.getChildByName('nv').active = friend.sex ==  '1'
            if(friend.status){
                item.getChildByName('bg').getChildByName('tag').getComponent(cc.Label).string = friend.status
            } else {
                item.getChildByName('bg').active = false;
            }
            if(btnText){
                item.getChildByName('goto').getComponentInChildren(cc.Label).string = btnText
            }
            
            global.Common.addButtonHandler(this,item.getChildByName('goto'),'onBtnGoTo',friend._id);
        }
    },
    onBtnGoTo(event,_id){
        cc.log('_id:',_id);
        global.EJMgr.popUI();
        this.selectCB&&this.selectCB(_id);
        
    },
    onEnterWindowBegin(){
        this.node.getChildByName('views').y = 1000;
        this.node.getChildByName('views').runAction(cc.moveTo(0.3,cc.v2(0,0)))
    },
    onExitWindowBegin(){
        this.node.getChildByName('views').y = 0;
        this.node.getChildByName('views').runAction(cc.moveTo(0.3,cc.v2(0,1000)))
    }

    // update (dt) {},
});
