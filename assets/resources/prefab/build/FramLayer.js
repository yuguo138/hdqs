var XINGZUO_NAME = ['大豆','玉米','高粱','马铃薯'];

cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        frameRes : cc.SpriteAtlas
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.no_panel = cc.find('views/panel',this.node);
        this.initWindowSize()
    },

    initWindowSize(){
        this.node.getChildByName('top').y = cc.winSize.height/2+150
        this.node.getChildByName('bottom').y = -706
        this.node.getChildByName('views').setScale(0.7)
    },

    onBtnPlant(event,data){
        var name = event.target.name;
        data = name.substring(4);
        cc.log('data:',data,name);
        var self = this;
        var item = this.no_panel.getChildByName(name);
        if(item.state == 3){ //摘取
            this.httpGet('/game/gameuser','island.vegetableField.harvest',{_id:this._id,index:data}, function(err,res){
                if(!err){
                    global.UI.ToolTip({message:'收取成功'})
                    self.updateData(res.fields);
                }
            });
        } else if(item.state > 0){
            this.httpGet('/game/gameuser','island.vegetableField.watering',{_id:this._id,index:data}, function(err,res){
                if(!err){
                    global.UI.ToolTip({message:'浇水成功'})
                    self.updateData(res.fields);
                }
            });
        } else { //种植
            this.cultivateIndex = data;
            var array = {0:'power',1:'water',2:'corn',3:'fire',4:'mineral',5:'potato',7:'soy',9:'wood',8:'sorghum'}
            
            var self = this;
            var seedLayer = global.EJMgr.pushUI('prefab/build/SelectSeed');
            seedLayer.getComponent('SelectSeed').setOptions(function(seed){
                global.EJMgr.popUI();
                cc.log('idx:',seed)
                this.httpGet('/game/gameuser','island.vegetableField.cultivate',{_id:self._id,index:self.cultivateIndex,type:seed}, function(err,res){
                    if(!err)
                        self.updateData(res.fields);
                });
            })
        }
    },
    onSelectFram(idx){
        global.EJMgr.popUI();
        if(!idx && idx != 0){
            return;
        }
        var seed = this.seedArray[idx];
        cc.log('idx:',seed)
        var index = this.cultivateIndex
        var self = this;
        this.httpGet('/game/gameuser','island.vegetableField.cultivate',{_id:this._id,index:this.cultivateIndex,type:seed.id}, function(err,res){
            if(!err)
                self.updateData(res.fields);
        });
    },

    onBtnCollect(){
        var idx = '';
        var self = this;
        for(var i=0; i<9; i++){
            var item = this.no_panel.getChildByName('item'+i);
            if(item.state == 3){
                if(idx.length != 0) idx += ','
                idx += i;
            }
        }
        if(idx.length == 0){
            global.UI.ToolTip({message:'目前没有可收获的植物'})
            return;
        }
        this.httpGet('/game/gameuser','island.vegetableField.harvest',{_id:this._id,index:idx}, function(err,res){
            if(!err)
                self.updateData(res.fields);
        });
    },
    onBtnAllPlant(event){
        var idx = '';
        var self = this;
        for(var i=0; i<9; i++){
            var item = this.no_panel.getChildByName('item'+i);
            if(item.state == 0){
                if(idx.length != 0) idx += ','
                idx += i;
            }
        }
        if(idx.length == 0){
            global.UI.ToolTip({message:'目前没有空地'})
            return;
        }
        var array = {0:'power',1:'water',2:'corn',3:'fire',4:'mineral',5:'potato',7:'soy',9:'wood',8:'sorghum'}
        global.EJMgr.pushUI('Eject/ListView')
        this.no_listView = global.EJMgr.getTopUI();
        this.seedArray = [];
        for(var key in global.DataConfig.seeds){
            var id = global.DataConfig.seeds[key].id;
            var count = global.Data.store[array[id]];;
            if(count > 0){
                var itemconfig = global.DataConfig.storage[id];
                this.seedArray.push({id:id,count:count,name:itemconfig.name});
            }
        }
        
        for(var i=0; i<this.seedArray.length; i++){
            this.no_listView.js.addItem(null,this.seedArray[i].name+' x '+this.seedArray[i].count);
        }
        var self = this;
        function batchPlant(type){
            global.EJMgr.popUI();
            if(!type){
                return;
            }
            self.httpGet('/game/gameuser','island.vegetableField.cultivate',{_id:this._id,index:idx,type:self.seedArray[type].id}, function(err,res){
                if(!err){
                    self.updateData();
                }
            });
        }
        var scrollviewheight = (this.seedArray.length +1)*50
        var point = event.target.convertToWorldSpace(cc.v2(45,scrollviewheight+150));
        this.no_listView.js.setOptions({
            x:point.x,
            y:point.y,
            height:scrollviewheight,
            callback : batchPlant
        })
    },
    onBtnWater(){
        var idx = '';
        var self = this;
        for(var i=0; i<9; i++){
            var item = this.no_panel.getChildByName('item'+i);
            if(item.state == 1 || item.state == 2){
                if(idx.length != 0) idx += ','
                idx += i;
            }
        }
        if(idx.length == 0){
            global.UI.ToolTip({message:'目前没有可浇水的植物'})
            return;
        }
        this.httpGet('/game/gameuser',' .vegetableField.watering',{_id:this._id,index:idx}, function(err,res){
            if(!err){
                self.updateData();
            }
        });
    },
    onBtnFriend(){
        cc.log('onBtnFriend')
        var self = this;
        this.httpGet('/game/gameuser','island.vegetableField.getFriendsListForField',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/build/FramFriends');
                node.getComponent('FramFriends').setOptions(res.friends,'好友列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },

    onBtnEnemy(){
        cc.log('onBtnEnemy')
        var self = this;
        this.httpGet('/game/gameuser','island.vegetableField.getEnemyListForField',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/build/FramFriends');
                node.getComponent('FramFriends').setOptions(res.enemy,'仇人列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },

    onBtnGotoFriend(_id){
        var framNode = global.UIMgr.pushUI('prefab/build/FramLayer');
        framNode.getComponent('FramLayer').setOptions({_id:_id, url:global.Data.headImgUrl,nickName:global.Data.nickname})
    },

    getSeedConfig(type){
        var seedInfo = global.DataConfig.seeds[type]

        for(var i=0; i<seeds.length; i++){
            if(seeds[i].id == id){
                return seeds[i]
            }
        }
        return null;
    },

    getSpriteFrame(type,isMiao){
        var str = isMiao ? 'miao_' : '';
        if(type == 2){
            str+='yumi'
        } else if(type == 5){
            str+='tudou'
        }else if(type == 7){
            str+='dadou'
        }else if(type == 8){
            str+='gaoliang'
        }
        return this.frameRes.getSpriteFrame(str);
    },

    updateSingleFram(idx,field){
        var panel = cc.find('views/panel',this.node);
        var item = panel.getChildByName('item'+idx);
        var action = cc.sequence(cc.delayTime(0.1),cc.fadeIn(0.2))
        item.getChildByName('tudi').runAction(action.clone())
        item.state = 0;
        var time = new Date().getTime();
        if(field.type >= 0){
            var seedInfo = this.getSeedCfg(field.type);
            var propInfo = global.DataConfig.storage[seedInfo.id];
            var sp = item.getChildByName('zhiwu').getComponent(cc.Sprite);
            sp.node.runAction(action.clone());

            var t = time - field.harvestTime

            if(time > field.harvestTime || (field.harvestTime - time) / seedInfo.harvestTime < 0.33){ //快成熟或已经成熟
                sp.spriteFrame = this.getSpriteFrame(propInfo.id,false)
                item.state = time > field.harvestTime?3:2
            } else { //还是苗
                sp.spriteFrame = this.getSpriteFrame(propInfo.id,true)
                item.state = 1;
            }
            
            item.getChildByName('bg').runAction(action.clone());
            item.getChildByName('name').runAction(action.clone());
            item.getComponentInChildren(cc.Label).string = propInfo.name;
        }
    },

    getSeedCfg(type){
        for(var key in global.DataConfig.seeds){
            var d = global.DataConfig.seeds[key];
            if(d.id == type){
                return d;
            }
        }
        console.warn('未找到种植配置:',type)
        return null;
    },

    updateData(fields){
        var panel = cc.find('views/panel',this.node);
        // var fields = global.Data.vegetableField;
        var time = new Date().getTime();
        for(var i=0; i<fields.length; i++){
            var field = fields[i];
            var item = panel.getChildByName('item'+i);
            var action = cc.sequence(cc.delayTime(0.1*i),cc.fadeIn(0.2))
            item.getChildByName('tudi').runAction(action.clone())
            var sp = item.getChildByName('zhiwu').getComponent(cc.Sprite);

            item.state = 0;
            if(field.type >= 0){
                var seedInfo = this.getSeedCfg(field.type);
                var propInfo = global.DataConfig.storage[seedInfo.id];
                sp.node.runAction(action.clone());
                var t = time - field.harvestTime
                var nodeaction = item.getChildByName('action')
                if(time > field.harvestTime || (field.harvestTime - time) / seedInfo.harvestTime < 0.33){ //快成熟
                    
                    sp.spriteFrame = this.getSpriteFrame(propInfo.id,false)
                    item.state = time > field.harvestTime?3:2
                    if(time > field.harvestTime){
                        // item.getChildByName('timebg').opacity = 0;
                        item.getChildByName('timepanel').opacity = 0;
                        nodeaction.getComponent(cc.Sprite).spriteFrame = this.frameRes.getSpriteFrame('shouqutishi')
                        nodeaction.runAction(action.clone())
                    } else {
                        nodeaction.getComponent(cc.Sprite).spriteFrame = this.frameRes.getSpriteFrame('shuihu')
                        if(field.waterMember.findIndex(global.Data._id)==-1){
                            nodeaction.runAction(action.clone())
                        } else {
                            nodeaction.opacity = 0;
                        }
                        item.getComponentInChildren('LastTimeRevert').setTime(field.harvestTime)
                    }
                } else { //还是苗
                    sp.spriteFrame = this.getSpriteFrame(propInfo.id,true)
                    item.state = 1;
                    nodeaction.getComponent(cc.Sprite).spriteFrame = this.frameRes.getSpriteFrame('shuihu')
                    if(field.waterMember.findIndex(global.Data._id)==-1){
                        nodeaction.runAction(action.clone())
                    } else {
                        nodeaction.opacity = 0;
                    }
                    item.getComponentInChildren('LastTimeRevert').setTime(field.harvestTime)
                    item.getComponentInChildren('LastTimeRevert').setTime(field.harvestTime)
                }
                
                item.getChildByName('bg').runAction(action.clone());
                item.getChildByName('name').runAction(action.clone());
                item.getComponentInChildren(cc.Label).string = propInfo.name;
            } else {
                sp.node.opacity = 0;
                item.getChildByName('timepanel').opacity = 0;
                item.getChildByName('action').opacity = 0;
                item.getChildByName('name').opacity = 0;
                item.getChildByName('bg').opacity = 0;
            }
        }
    },

    onEnterWindowBegin(){
        cc.log('onEnterWindowBegin',this)
        this.initWindowSize()
        var f = true;
        this.node.getChildByName('top').runAction(cc.sequence(cc.delayTime(0.2),cc.moveTo(0.15,cc.v2(0,cc.winSize.height/2))));
        this.node.getChildByName('bottom').runAction(cc.sequence(cc.delayTime(0.4),cc.moveTo(0.15,cc.v2(0,-502))))
        this.node.getChildByName('bg').runAction(cc.fadeTo(0.15,150))

        var actions = this.getEjectAction();
        actions.push(cc.callFunc(this.onEnterWindowEnd.bind(this)))
        this.node.getChildByName('views').runAction(cc.sequence(actions))

        var self = this;
        var time = new Date().getTime();
        this.httpGet('/game/gameuser','island.vegetableField.getVegetableFieldMessage',{_id:this._id}, function(err,res){
            // cc.log('菜地信息:',res)
            var ct = new Date().getTime();
            if(ct - time < 300){
                //setTimeout(self.updateData.bind(self,res),300-ct-time);
                self.scheduleOnce(self.updateData.bind(self,res.fields),(300-ct-time)/1000)
            } else {
                self.updateData(res.fields);
            }
        });
    },

    onExitWindowBegin(){
        cc.log('onExitWindowBegin',this)

        // this._super();
        this.node.getChildByName('top').runAction(cc.sequence(cc.delayTime(0.2),cc.moveTo(0.15,cc.v2(0,cc.winSize.height/2+150))));
        this.node.getChildByName('bottom').runAction(cc.sequence(cc.delayTime(0.3),cc.moveTo(0.15,cc.v2(0,-706))))
        this.node.getChildByName('bg').runAction(cc.fadeOut(0.15,150))

        this.node.getChildByName('views').runAction(cc.scaleTo(0.2,0));

        global.DataUpdate.updateBag();
    },

    setOptions(option){
        this._id = option._id;
        cc.find('top/icon',this.node).getComponent('ImageLoader').setOptions({url:option.url});
        cc.find('top/label',this.node).getComponent(cc.Label).string = option.nickName;
    }

    // update (dt) {},
});
