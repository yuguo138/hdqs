
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.lb_title = cc.find('view/title',this.node).getComponent(cc.Label)
        this.lb_button = cc.find('view/build',this.node).getComponent(cc.Button)
    },

    start () {

    },

    onBtnBuild(){
        var obj = {};
        !!this.n_buildId && (obj.part = this.n_buildId);
        var self = this;
        global.Http.get('/game/gameuser',this.route?this.route:'island.build',obj, function(err,res){
            if(err){
                cc.log('建造失败')
                return;
            }
            self.cb_Build&&self.cb_Build(self.route,self.n_buildId);
            global.EJMgr.popUI();
        })
    },

    setOptions(option){

        this.lb_title.string = option.title;
        this.cb_Build = option.cb;
        this.route = option.route;
        this.n_buildId = option.part;
        this.o_use = option.use;
        if(option.part == 'water'){
            cc.find('view/panel/item0',this.node).getComponent(cc.Label).string = '水:'+option.use[0]
            cc.find('view/panel/item1',this.node).active = false
            cc.find('view/panel/item2',this.node).active = false;
        } else {
            cc.find('view/panel/item0',this.node).getComponent(cc.Label).string = '木材:'+option.use[0]
            cc.find('view/panel/item1',this.node).getComponent(cc.Label).string = '矿石:'+option.use[1]
            cc.find('view/panel/item2',this.node).getComponent(cc.Label).string = '火种:'+option.use[2]
        }
        
    }
    // update (dt) {},
});
