
require('wy-Prototype')

cc.Class({
    extends:require('wy-Component'),

    properties: {
        uiMgr : require('UIManager'),
        ejMgr : require('EjectManager'),
        gameLoop :require('GameLoop'),
        config : cc.RawAsset,
        resJson : cc.RawAsset
    },

    onLoad () {
        if(window.global == null){
            window.global = {}
        }
        global.Config = require('wy-Config')
        global.Common = require('wy-Common');
        global.Audio = require('wy-Audio')
        global.Http = require('wy-Http')
        global.Wechat = require('wy-WeChat');
        global.DataMgr = require("wy-DataManager");
        global.ChatMsgQueue = [];
        global.Loader = require('wy-Loader')
        global.UIMgr = this.uiMgr;
        global.EJMgr = this.ejMgr;
        global.Event = require('wy-EventEmiter');
        global.GameLoop = this.gameLoop;
        global.DataConfig = null //游戏的数据配置
        window.CCAsync = require('CCAsync')
        global.UI = require('wy-UI')
        global.DataUpdate = require('GlobalDataUpdate')
        global.Struct = require('Struct')
        global.Data = {};
        window.SoundMgr = require('SoundMgr')
        SoundMgr.init();
    },

    start () {

        global.isVersion2 = cc.ENGINE_VERSION > '2.0.0'
        
        cc.loader.load(this.resJson,function(err,res){
            if(cc.ENGINE_VERSION > '2.0.0'){
                global.Resources = res.json
            } else {
                global.Resources = res
            }
            
            global.UIMgr.loadLayer('Login',0)
        })





        
        cc.game.setFrameRate(40)
    
        // global.Loader.loaderScene('Login')
        
        // var url = 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKVEyX2hqUPnshYEiarvhh1FtybiapVsBf4SY8ibJy8X6ial9LXUYkfLY0w5JicHHyOAFZUMS8g3zicibDvA/132'
        // cc.loader.load({ url: url, type: 'png' }, (err, texture) => {
        //     cc.log(texture);
        // });
    },

    update (dt) {
        //cc.log('darwcall:',cc.renderer.drawCalls)
    },
});
