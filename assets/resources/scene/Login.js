
let AcTimer = [0, 1000];
var openids = ['os1go42W4RCublP60JkGkbxXpPYA','tesetx0HsngE4F7h6veFal8s8wQCM','5b9cc7bfaca2a037782e072e']
var tokens = ['G+x/xdigLOFYGaH7pqtAZA==','GbkEC27XLsJyyVT5xyRM0w==','GbkEC27XLsJyyVT5xyRM0w==']

var WxFileRead = require('WxFileRead')
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    onLoad: function () {
    },

    start: function () {
        var obj = typeof(window.wx);
        if(CC_WECHATGAME){
            this.node.getChildByName('guest').active = false;
            global.Wechat.login(this.weChatLogin.bind(this));
        } else {
            window.wx = {};
        }
        wx.RankKey = 'level'
    },

    cleanResource(){
        global.Wechat.cleanResource()
    },

    weChatLogin: function (data) {
        cc.log('weChatLogin:',data);
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        if(data.code){
            this.httpGet('/game/gameUser/wxLogin', '', data, this.toLogin.bind(this));
        } else {
            var openid = cc.sys.localStorage.getItem('weiopenid');
            var token = cc.sys.localStorage.getItem('weitoken');
            this.toLogin(null,{openid:openid,token:token})
        }
    },

    onLogin: function (event,data) {
        var idx = parseInt(data);
        var d = {openid:openids[idx],token:tokens[idx]};
        
        this.toLogin(null,d)
    },

    toLogin(err,data){
        global.Data.openid = data.openid
        global.Data.token = data.token
        global.Http.setOptions({openid:global.Data.openid,token:global.Data.token})

        var self = this;

        this.httpGet('/game/gameuser','getMessage',{latitude:self.latitude,longitude:self.longitude}, function(err,res){
            if(err){
                global.UI.Alert({title:'提示',message:'缓存有错，需要重新登陆',okButtonCallback:function(){
                    global.Wechat.cleanResource()
                }})
                return
            }
            Object.assign(global.Data,res);
            console.log('登陆数据:',global.Data);
            // var url = 'http://10.0.0.16/game/gameUser/gameConfig?openid=os1go42W4RCublP60JkGkbxXpPYA&token=G%2Bx%2FxdigLOFYGaH7pqtAZA%3D%3D'
            var url = global.Data.configUrl+'?a='+new Date().getTime()
            WxFileRead.getConfigFile('config.json',global.Config.httpRootUrl+url,global.Data.configVersion,function(err,res){
                console.log('配置数据:',global.Data);
                if(err){
                    global.UI.Alert({title:'提示',message:'未获取到配置文件，需要重新登陆',okButtonCallback:function(){
                        global.Wechat.cleanResource()
                    }})
                } else {
                    global.DataConfig = res.data.gameConfig
                    self.loginOK(null,data);
                }
            })
        });
    },
    onDestroy(){
        console.log('')
    },

    loginOK: function (err,data) {
        cc.log('======>>登录成功!', data,global.DataConfig);
        cc.sys.localStorage.setItem('weiopenid',data.openid);
        cc.sys.localStorage.setItem('weitoken',data.token);

        global.UIMgr.popUI();
        global.Loader.loadResources('GameLoop',function(err,res){
            global.GameLoop.startLoop();
        })

        if(CC_WECHATGAME){
            wx.postMessage({
                messageType : -1,
                openId : data.openid
            })
        }
    }

    

    // update (dt) {},
});
