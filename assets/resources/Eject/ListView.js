
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        listItem : cc.Prefab,
        content :cc.Node
    },
    onLoad(){
        this._super();
        this.isAnimate = false;
        this.endHeight = 0;
        this.no_mask = cc.find('mask/null',this.node)
        // this.js_view1 = cc.find('mask/view',this.node).getComponent(cc.Widget);
        // this.js_view2 = cc.find('mask/view/view',this.node).getComponent(cc.Widget);

        this.no_mask.y = 360;
    },
    addItem(icon ,text){
        var list = cc.instantiate(this.listItem);
        list.tagindex = this.content.children.length;
        this.content.addChild(list);
        icon && (list.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = icon);
        text && (list.getChildByName('text').getComponent(cc.Label).string = text);
        list.on(cc.Node.EventType.TOUCH_END,this.onClickEnd,this);
    },

    onClickEnd(event,data){
        cc.log('onClickEnd:',event.target.tagindex)
        this.callback && this.callback(event.target.tagindex);
    },

    onBtnClose(){
        this.callback && this.callback(this.defaultIndex);
    },

    setOptions(options){
        options.callback && (this.callback = options.callback);
        if(options.x && options.y){
            var p = this.node.convertToNodeSpace(cc.v2(options.x,options.y));
            p.x += 10-this.no_mask.width/2
            p.y += 8
            cc.find('mask',this.node).setPosition(p);
        }
        options.default && (this.defaultIndex = options.default);
        options.height && (this.endHeight = options.height);
    },

    //即将打开窗口 (前面的窗口需要多久的动画时间)
    onEnterWindowBegin(time){
        this.no_mask.runAction(cc.moveTo(0.3,cc.v2(0,0)))
        // this.no_mask.height = 0;
        // !this.endHeight&&(this.endHeight = 360);
        // this.isAnimate = true;
        // this.delayHeight = this.endHeight * 16 / (0.3*1000);
    },
    //打开窗口完成
    onEnterWindowEnd(){
        cc.log('onEnterWindowEnd')
    },
    //即将关闭窗口 告诉别人你要多久才能关闭
    onExitWindowBegin(){
        this.no_mask.runAction(cc.moveTo(0.3,cc.v2(0,360)))
        // this.endHeight = 0;
        // this.isAnimate = true;
        // this.delayHeight = -this.no_mask.height * 16 / (0.3*1000);
        return 0.3;
    },
    //关闭窗口完成
    onExitWindowEnd(){
        
    },

    removeFromUI(){
        global.UIMgr.popUI();
    },

    update(){
        if(this.isAnimate){
            var max = this.endHeight > this.no_mask.height;
            if(max){
                this.no_mask.height += this.delayHeight;
                if(this.endHeight < this.no_mask.height){
                    this.no_mask.height = this.endHeight;
                    this.isAnimate = false;
                    this.onEnterWindowEnd();
                }
            } else {
                this.no_mask.height += this.delayHeight;
                if(this.endHeight > this.no_mask.height){
                    this.no_mask.height = this.endHeight;
                    this.isAnimate = false;
                    this.onEnterWindowEnd();
                }
            }
            // this.js_view1.updateAlignment();
            // this.js_view2.updateAlignment();
        }
    }

});
