
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
       item:cc.Prefab,
       content : cc.Node,

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
    },
    set(){
        for(let i=0;i<5;i++){
            let item = cc.instantiate(this.item);
            this.content.addChild(item);

            item.getComponent('SettingItem').setOptions(i)

            item.x = -550
            item.runAction(cc.sequence(cc.delayTime(0.2+i*0.1),cc.moveTo(0.2,cc.v2(0,0))))
        }
    },
    start () {
        var self = this;
        var async = new CCAsync();
        async.parallel([function(cb){
            var f = true;
            for(var i=0; i<self.node.children.length; i++){
                var chd = self.node.children[i];
                if(chd.name == 'bg' || chd.name == 'mask'){
                    chd.runAction(cc.fadeTo(0.15,150));
                } else {
                    var actions = self.getEjectAction();
                    if(f){
                        actions.push(cc.callFunc(function(){
                            cb(null,1)
                        }))
                        f = false;
                    }
                    
                    chd.runAction(cc.sequence(actions))
                    chd.runAction(cc.fadeIn(0.2));
                }
            }
        }],function(){
            self.set()
        })
    },

    // update (dt) {},
});
