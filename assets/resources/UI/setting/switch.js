const Event=require('wy-EventEmiter')
cc.Class({
    extends: cc.Component,

    properties: {
        btn:cc.Node,
        bg:cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.on(cc.Node.EventType.TOUCH_END, ()=>{
            this.btn.runAction(cc.sequence(cc.moveTo(0.2,cc.v2(-20,0)),cc.callFunc(()=>{
                if(this.btn.x<-30){
                    this.bg.color=new cc.Color(255,255,255);
                    Event.emit('OFF')
                }else{
                    this.bg.color=new cc.Color(0,100,255);
                    Event.emit('ON')
                }
            })))
        }, this);
    },

    start () {

    },

    // btn(){
    //     this.btn.runAction(cc.moveTo(0.5,cc.v2(-2,0)))
    // }
    // update (dt) {},
});
