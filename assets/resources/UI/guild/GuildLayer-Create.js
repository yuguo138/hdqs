
const cosntLevel = [0,150,200,250,300,350,400,500,600,700,800,900,1000]
cc.Class({
    extends: require('wy-Component'),

    properties: {
        applyConst : require('CrossChoose'),
        levelConst : require('CrossChoose'),
        editText : cc.EditBox,
        scrollSelect : require('ScrollSelect')
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.applyConst.setOptions({
            default : 1,
            data : ['自由加入','需要验证后加入','不允许加入']
        })
        this.levelConst.setOptions({
            default : 0,
            data : cosntLevel
        })
    },

    onChangeConst(e,d){
        cc.log(e.getIndex());
    },

    onBtnCreate(){
        var data = {
            name : this.editText.string,
            joinPower : this.applyConst.getIndex(),
            leastCivilizationLevel : cosntLevel[this.levelConst.getIndex()],
            icon : this.scrollSelect.getIndex()
        }
        cc.log('data:',data);
        this.httpGet('/game/gameUser','sociality.createGuild',data,function(err,res){
            cc.log('res:')
        })
    },

    start () {

    },

    // update (dt) {},
});
