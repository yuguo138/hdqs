
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        pageView : require('PackageView'),
        guildItem : cc.Prefab,
        gulidChat : require('GuildMain-Chat'),
        guildLobby : require('GuildMain-Lobby')
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initWindowSize();
        this._super();
        this.initPageId = 0

        var self = this;
        this.httpGet('/game/guild','getMessage',{}, function(err,res){
            if(!err){
                self.guildLobby.updateGuildInfo(res);
                self.gulidChat.updateGuildInfo(res)
            }
                
        });
        this.httpGet('/game/guild','getSelfMessage',{}, (err,res)=>{
            if(!err){
                // cc.log('getSelfMessage:',res)
                cc.find('coinbg/label',self.guildLobby.node).getComponent(cc.Label).string = res.self.coin
                global.Data.guild.selfLevel = res.self.level
                // self.level = res.self.level;
            }
        });  
    },

    //切换徽章
    onChangeBadge(scollselect,index){
        cc.log('onChangeBadge:',index)
    },

    onChangePackage(packview,viewid){
        cc.log('onChangePackage:',viewid)
    },

    onEnterWindowBegin(){
        this._super();
        //request data
        this.pageView.setOptions({pageId:this.initPageId});
    },

    onEnterWindowEnd(){
        // this.pageView.setOptions({scrollEnable:false,pageId:1})
    },

    setOptions(pageId){
        this.initPageId = pageId
        
    }
});
