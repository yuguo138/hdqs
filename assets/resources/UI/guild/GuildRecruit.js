
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        editBox : cc.EditBox
    },


    onBtnSend(){

        var obj = {
            channel :'world',
            type:global.Struct.ChatType.Recruit,
            content : JSON.stringify({
                guildId : global.Data.guildId,
                message : this.editBox.string,
            })
        }
        cc.log('obj',obj);
        this.httpGet('/game/gameUser/chat','send',obj,function(err,res){
            cc.log('发送请求成功',res);
            global.UI.ToolTip({message:'发送邀请成功'})
            global.EJMgr.popUI();
        })
    }

    // update (dt) {},
});
