
cc.Class({
    extends: require('ChatCommon'),

    properties: {
        GuildChatMsg : cc.Prefab
    },

    onLoad () {
        
        this.isPageShow = false;
    },

    updateGuildInfo(guild){
        this.setChannel('guild'+guild.id);
    },

    instantiateItem(msg,lasttime,idx){
        if(msg.type < 100){
            return this._super(msg,lasttime,idx);
        } else {
            var node = cc.instantiate(this.GuildChatMsg);
            var layout = node.getChildByName('layout')
            var i = msg.content.indexOf('xxx');
            if(i>0){
                var s = msg.content.substring(0,i);
                layout.getChildByName('start').getComponent(cc.Label).string = s;
            } else {
                layout.getChildByName('start').active = false;
            }
            layout.getChildByName('text').getComponent(cc.Label).string = msg.nickname;

            if(i<msg.content.length-3){
                var s = msg.content.substring(i+4);
                layout.getChildByName('end').getComponent(cc.Label).string = s;
            } else {
                layout.getChildByName('end').active = false;
            }

            return node;
        }
    },

    updateMessage(){
        if(this.isPageShow){
            this._super();
        }
    },

    sendMessage(){
        // cc.log('str:',this.editText.string);
        var self=this;
        this.sendObject({type:0,content:this.editText.string},function(err,res){
            if(!err){
                self.editText.string='';
                global.UI.ToolTip({message:'消息发送成功'})
            }
        });
    },

    onPageShow(){
        this.isPageShow = true;
    },
    onPageHide(){
        this.isPageShow = false;
    }
});
