
cc.Class({
    extends: require('wy-Component'),

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
    },

    onRemove(){
        this.node.active = false;
    },
    onPageShow(){
        for(var i=0; i<this.node.children.length; i++){
            this.node.children[i].active =true;
        }
    },
    onPageHide(){
        for(var i=0; i<this.node.children.length; i++){
            this.node.children[i].active =false;
        }
    }
});
