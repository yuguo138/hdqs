
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },
    onLoad(){
        this._super();
        this.node.getChildByName('views').x = -720;
    },

    start () {
        this.pageView = this.node.getChildByName('views').getChildByName('PackageView').getComponent('PackageView');
    },

    onEnterWindowBegin(){
        this.node.getChildByName('views').runAction(cc.sequence(cc.moveTo(0.2,cc.v2(0,0)),cc.callFunc(this.onEnterWindowEnd.bind(this))));
    },
    onEnterWindowEnd(){
         var panel = this.pageView.getPages()[this.pageView.getCurrentPageIndex()];
        panel.js && panel.js.onShowFull && panel.js.onShowFull();
    },
    onExitWindowBegin(){
        this.node.getChildByName('views').x = 0;
        this.node.getChildByName('views').runAction(cc.moveTo(0.2,cc.v2(-720,0)))
        this.node.getChildByName('bg').runAction(cc.sequence(cc.delayTime(0.2),cc.fadeOut(0.2)))
    },

    // update (dt) {},
});
