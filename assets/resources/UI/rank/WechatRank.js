
cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.rankSprite = this.node.getComponent(cc.Sprite);
        this.rankTexture = new cc.Texture2D();
        this.rankSpriteFrame = new cc.SpriteFrame();
        
        this.tex = new cc.Texture2D();
        
        this.node.width = cc.winSize.width;
        this.node.height = cc.winSize.height

        this.node.y += (cc.winSize.height-1280)/2
    },

    onRemove(){
        // this.node.destroy();
        this.node.active = false;
    },
    start () {

    },

    onEnable(){
        if(window.sharedCanvas){
            window.wx.postMessage({
                messageType: 2,
                openType: wx.RankKey
            });
        } else {

        }
    },
    onDestroy(){
        delete this.rankTexture
        delete this.rankSpriteFrame;
        cc.log('onDestroy')
        if(CC_WECHATGAME){
            window.wx.postMessage({
                messageType: 0
            });
        }
    },

    _updateShareFrame(){
         if (window.sharedCanvas != undefined) {
            if(this.rankSpriteFrame){
                delete this.rankSpriteFrame
            }
            this.tex.initWithElement(window.sharedCanvas);
            this.tex.handleLoadedTexture();
            this.rankSpriteFrame = new cc.SpriteFrame(this.tex);
            this.rankSprite.spriteFrame = this.rankSpriteFrame
        }
    },

    update (dt) {
        this._updateShareFrame();
    },
});
