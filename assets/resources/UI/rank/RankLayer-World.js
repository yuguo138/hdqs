
cc.Class({
    extends: require('wy-Component'),

    properties: {
        rankItem : cc.Prefab,
        content :cc.Node,
        scrollView : cc.ScrollView,
        selfItem : cc.Node
    },

    onLoad(){
        this.rankPage = 1;
        this.rankDatas = [];
    },

    onEnable(){
        this._super();
        this.scrollView.getScrollOffset();
        this.scrollView.node.on("bounce-bottom", this.scrollToBottom, this);
    },
    onDisable(){
        this.scrollView.node.off("bounce-bottom");
    },

    scrollToBottom(){
        cc.log('scrollToBottom')
        this.onPageShow();
    },

    updateUI(){
        for(var i=0; i<this.rankDatas.length; i++){
            var data = this.rankDatas[i];
            if(!data.isShow){
                var node = cc.instantiate(this.rankItem)
                node.getComponent('UserInfo').setOptions(data.user);
                node.getChildByName('rank').getComponent(cc.Label).string = data.ranking;
                this.content.addChild(node);
                data.isShow = true;
                
            }
        }
        this.node.getChildByName('RankItem').getComponent('UserInfo').setOptions(global.Data);
        cc.find('RankItem/rank',this.node).getComponent(cc.Label).string = this.selfRank+1;
        cc.find('RankItem/level',this.node).getComponent(cc.Label).string = this.rankDatas[this.selfRank].user.civilizationLevel
    },

    onPageShow(){
        var self = this;
        this.httpGet('/game/gameUser/rankingList','',{page : this.rankPage,myself:this.selfRank},function(err,res){
            cc.log('res',res);
            if(err){
                return;
            }
            if(res.gameUsers.length > 0){
                self.rankPage = res.page+1
                self.selfRank = res.myself;
                Object.assign(self.rankDatas,res.gameUsers);
                self.updateUI();
            } else {
                self.scrollView.node.off("bounce-bottom");
            }
        })
    }

});
