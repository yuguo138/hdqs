
cc.Class({
    extends: require('wy-Component'),

    properties: {
        toolFont: cc.Label,
        _options: {
            default: {}
        },
    },

    onLoad: function () {
        this._options = {
            y: 0,
            message: '这是一个警告框',
            time: 1.5,
            callback: () => { }
        }
    },

    start: function () {

    },

    setOptions: function (data) {
        Object.assign(this._options, data)
        this.toolFont.string = this._options.message;
        this.node.runAction(cc.sequence(
            cc.fadeIn(0.2),
            cc.delayTime(this._options.time),
            cc.fadeOut(0.5),
            cc.callFunc(() => {
                if (!!this._options.callback)
                    (this._options.callback)();
                this.node.destroy();
            })
        ));
    },

    // update (dt) {},
});
