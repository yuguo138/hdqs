
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.discard = cc.find('views/discard',this.node).getComponent(cc.Button);
        this.selectIndex = 0;
        this.updateColor();
        this.updateButton();
    },

    updateColor(){
        for(var i=0; i<6; i++){
            var t = cc.find('views/food/item'+i,this.node);
            if(i == this.selectIndex){
                t.color = new cc.Color(186,252,701)
                // this.icon.spriteFrame = t.getChildByName('icon').getComponent(cc.Sprite).spriteFrame
            } else {
                t.color = cc.Color.WHITE
            }
        }
    },
    updateButton(){
        this.discard.interactable = global.Data.driftBottle.discardCount>0
    },

    onBtnWish(){
        var self = this;
        this.httpGet('/game/gameUser','sociality.driftBottle.discard',{goodsId:this.selectIndex},function(err,res){
            global.Data.driftBottle.discardCount--;
            self.updateButton();
            // cc.log('')
        })
    },
    onBtnSelect(event,data){
        this.selectIndex = data;
        this.updateColor();
    }

    // update (dt) {},
});
