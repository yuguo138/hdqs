
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        item :cc.Prefab,
        pageView : require('PackageView')
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
    },
    start(){
        
    },

    onBtnChange(){
        var data = [];
        var self = this;
        this.httpGet('/game/chat','friends.rcmd',{},function(err,res){
            self.updateAround(res.nearUser)
        })
    },

    updateAround(data){
        var panel = this.pageView.getPages()[this.pageView.getCurrentPageIndex()];
        panel.removeAllChildren();
        let arr=[cc.v2(-145,245),cc.v2(155,245),cc.v2(-145,-110),cc.v2(155,-110)]
        for(var i=0; i<data.length; i++){
            var d = data[i];
            var item = cc.instantiate(this.item);
            
            panel.addChild(item);
            item.setPosition(arr[i])
            item.opacity = 0;
            item.scale = 0.7;
            item.runAction(cc.sequence(cc.delayTime(0.2*i),cc.spawn(cc.scaleTo(0.2,1.05),cc.fadeIn(0.2)),cc.scaleTo(0.1,1.0)))
            item.getComponent('UserInfo').setOptions(d);
            global.Common.addButtonHandler(this,item.getChildByName('add'),'onBtnAdd',i);
        }
    },
    onBtnAdd(event,d){
        cc.log('onBtnAdd:',d)
    },
    onEnterWindowEnd(){
        this._super();
    },
    setOptions(options){
        this.pageView.setOptions({pageId:0})
        this.updateAround(options.nearUser);
        this.near=options.nearUser;
    }

    // update (dt) {},
});
