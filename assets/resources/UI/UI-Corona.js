
cc.Class({
    extends: require('wy-Component'),

    properties: {
        lpBg: cc.Node,  //轮盘背景
        lpBtn: cc.Node, //轮盘按钮
        _isFo: true,    //轮盘是否随动
        _px: 0,         //触摸点位置x
        _py: 0,         //触摸点位置y
        _bx: 0,         //轮盘位置x
        _by: 0,         //轮盘位置y
        _xx: 0,         //按钮位置x
        _xy: 0,         //按钮位置y
        _r: 0,          //半径(力度)
        _sin: 0,
        _cos: 0,
        _maxR: 100,
    },

    onLoad() {
        // this.lpBg.active = false;
        // this.lpBtn.active = false
        this.canvas = cc.find('Canvas');
        this.node.on('touchstart', this.onStart, this);
        this.node.on('touchmove', this.onMove, this);
        this.node.on('touchend', this.onEnd, this);
        this.node.on('touchcancel', this.onEnd, this);
    },

    start() {

    },

    //触摸三连击
    onStart: function (e) {
        this.reset();
        this.updatePos(e);
        this.setBgPos();
        this.setData();
        this.moveBtn();
    },
    onMove: function (e) {
        this.updatePos(e);
        this.setData();
        this.moveBtn();
    },
    onEnd: function (e) {
        this.updatePos(e);
        this.setData();
        this.reset();
    },

    //更新触摸点实时位置
    updatePos: function (e) {
        var pos = e.getLocation();
        this._px = pos.x - this.canvas.width / 2 + this.node.x;
        this._py = pos.y - this.canvas.height / 2 + this.node.y;
    },

    //数据赋值
    setData: function () {
        this._xx = this._px - this._bx;
        this._xy = this._py - this._by;
        this._r = Math.sqrt(this._xx * this._xx + this._xy * this._xy);
        if (this._r > this._maxR) {
            var m = 1 - (this._r - this._maxR) / this._r;
            this._xx = this._xx * m;
            this._xy = this._xy * m;
            this._r = this._maxR;
        }
        this._sin = this._xx / this._r;
        this._cos = this._xy / this._r;
        //这里把 r 和 x y 四舍五入了
        //如果不需要 注释 即可
        this._r = Math.round(this._r);
        this._xx = Math.round(this._xx);
        this._xy = Math.round(this._xy);
        cc.log(this._xx, this._xy, this._r, this._sin, this._cos);
    },

    //根据 按钮修正位置 移动按钮
    moveBtn: function () {
        this.lpBtn.setPosition(this._xx, this._xy);
    },

    //获取轮盘实时数据
    getData: function () {
        var obj = {
            x: this._xx,    //按钮修正坐标x
            y: this._xy,    //按钮修正坐标y
            r: this._r,     //半径（力度）
            sin: this._sin,
            cos: this._cos,
        }
        return obj;
    },

    //设置轮盘初始坐标
    setBgPos: function () {
        if (this._isFo) {
            this.lpBg.setPosition(this._px, this._py);
            this._bx = this._px;
            this._by = this._py;
        }
    },

    //修改轮盘随动
    setIsFo: function (boo) {
        this._isFo = !!boo;
    },

    reset: function (isAll) {
        this.lpBg.setPosition(0, 0);
        this.lpBtn.setPosition(0, 0);
        this._px = this._py = this._bx = this._by = this._xx = this._xy = this._r = this._sin = this._cos = 0;
    },

    // update (dt) {},
});
