cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        loginReward : cc.Node,
        dayReward : cc.Node,
        freeReward : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initWindowSize();
        this._super();
        
        this.cid = 0;
        this.lastChild = null;
        this.pageView = this.node.getChildByName('views').getComponent('PackageView');
        this.b_isFirstShow = true;
        this.showLoginReward();
    },

    showLoginReward(){
        // var node = 
        var dayRewardConfig = global.DataConfig.dayReward;
        cc.log('===========',dayRewardConfig)
        for(var i=0; i<6; i++){
            var item = cc.find('items/item'+i,this.loginReward);
            item.getChildByName('yilingqu').active = i < global.Data.continueDay
            item.color = (i+1 != global.Data.day ? cc.Color.WHITE : new cc.Color(241,187,48))
            item.getChildByName('tequan').active = dayRewardConfig[i].vip >=0
            item.getChildByName('prop').getComponent('Prop').setId(dayRewardConfig[i].id,dayRewardConfig[i].count);
            // var p = global.DataConfig.storage[dayRewardConfig[i].id];
            // var lb = new cc.Node().addComponent(cc.Label);
            // item.addChild(lb.node);
            // lb.node.color = new cc.Color(0,0,0)
            // lb.node.y = -10
            // lb.string = p.name+'\n'+dayRewardConfig[i].count;
        }
        this.loginReward.getChildByName('get').getComponent(cc.Button).interactable = global.Data.day>=0;
    },

    showDayReward(){
        var day = new Date();
        var ld = new Date(global.Data.dayPowerTime);
        var get = false;
        if((day.getHours() >= 12 && day.getHours() < 14) && (ld.getHours() < 12 || day.getHours() >= 14)){
            get = true;
        } else if((day.getHours() >= 18 && day.getHours() < 20) && (ld.getHours() < 18 || day.getHours() >= 20)){
            get = true;
        }

        this.dayReward.getChildByName('btn').getComponent(cc.Button).interactable = get
    },

    showFreeReward(){
        var enable = global.Data.freePowerCount > 0 ;
        var idx = enable ? 2 : global.Data.freePowerCount
        var reward = global.DataConfig.freeReward[2-idx]
        var rewardProp = this.freeReward.getChildByName('prop').getComponent('Prop')
        rewardProp.setId(reward.id,reward.count)
        this.freeReward.getChildByName('btn').getChildByName('prop').getComponent('Prop').setId(reward.id,reward.count)
        this.freeReward.getChildByName('btn').getComponent(cc.Button).interactable = enable > 0;
    },

    onBtnDayReward(event){
        cc.log('')
        var day = global.Data.day
        var self = this
        this.httpGet('/game/gameUser','dayReward.getReward',{},function(err,res){
            global.Data.continueDay = day;
            self.showLoginReward();
        })
    },

    onChangePage(event,d){
        // cc.log('onChangePage:',d);
        if(d == 0){
            this.showLoginReward();
        } else if(d == 1){
            this.showDayReward();
        } else if(d == 2){
            this.showFreeReward();
        }
    },

    onBtnEveryPower(){
        var day = new Date();
        var ld = new Date(global.Data.dayPowerTime);
        var get = false;
        
        if((day.getHours() >= 12 && day.getHours() < 14) && (ld.getHours() < 12 || day.getHours() >= 14)){
            get = true;
        } else if((day.getHours() >= 18 && day.getHours() < 20) && (ld.getHours() < 18 || day.getHours() >= 20)){
            get = true;
        }
        if(!get){
            return this.dayReward.getChildByName('btn').getComponent(cc.Button).interactable = false
        }
        var self = this
        this.httpGet('/game/gameUser','dayReward.getReward',{},function(err,res){
            global.Data.dayPowerTime = day.getTime();
            self.showDayReward();
        })
        
    },

    onBtnFreePower(){
        var rewardProp = this.freeReward.getChildByName('prop').getComponent('Prop')
        var sf = rewardProp.getSpriteFrame()
        var startPos = rewardProp.node.convertToWorldSpace(cc.v2(50,0))
        var self = this
        this.httpGet('/game/gameUser','freeReward.getReward',{},function(err,res){
            global.UI.ToolTip({message:'领取分享奖励成功'})
            var cf = global.DataConfig.freeReward[res.freeCount];
            var item = global.DataConfig.storage[cf.id]
            var node = global.EJMgr.pushUI('prefab/gameReady/RewardResult');
            var resultPanel = node.getComponent('RewardResult');
            
            var layer = global.GameLoop.getMenuBtnsLayer();
            var endpos = layer.getChildByName('pack').getPosition()
            var packPos = layer.convertToWorldSpace(endpos)
            resultPanel.setOptions({sf:sf,
                endPos:packPos,
                startPos:startPos,
                name:item.name+'+'+cf.count
            })
            global.Data.freePowerCount = res.freeCount;
            self.showLoginReward();
        })
        if(CC_WECHATGAME){
            global.Wechat.share('免费能量',{openid:global.Data.openid},function(){

            },wxDownloader.REMOTE_ROOT+'wechatshare.png')
        }
        
    },

    onChangePageEvent(page,viewId){
        // var panel = this.pageView.getPages()[viewId];
        // panel.emit('onWindowFocus')
    },

    onEnterWindowBegin(){
        this._super();
        this.pageView.setOptions({pageId:0});
        this.b_isFirstShow = false;
    },

    onEnterWindowEnd(){
        // var panel = this.pageView.getPages()[this.pageView.getCurrentPageIndex()];
        // panel.js.onWindowFocus&&panel.js.onWindowFocus();
    },

    onWindowFocus(){ //从别的界面退回来时激活主界面时，传递到子界面去
        var d = this.pageView.getCurrentPageIndex()
        if(d == 0){
            this.showLoginReward();
        } else if(d == 1){
            this.showDayReward();
        } else if(d == 2){
            this.showFreeReward();
        }
    }
    // update (dt) {},
});
