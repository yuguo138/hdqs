
cc.Class({
    extends: require('wy-Component'),

    properties: {
        scrollView:cc.ScrollView,
        emailItem :cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.messages = null;
        this.emailPage = 1;
    },

    onEnable(){
        this._super();
        // this.scrollView.node.on("bounce-top", this.scorllToTop, this);
        // this.scrollView.node.on("bounce-bottom", this.scrollToBottom, this);
    },
    onDisable(){
        // this.scrollView.node.off("bounce-top",);
    },

    scorllToTop(){
        if(this.messages){
            return;
        }
        var self = this;
        this.httpGet('/game/gameUser','email.list',{page:this.emailPage},(err,res)=>{
            if(err){
                return
            }
            self.messages = res.emails
            // for(var i=0; i<res.emails.length; i++){
            //     self.messages.push(res.emails[i]);
            // }
            self.addEmail();
        })
    },

    addEmail(){
        this.emailPage ++;
        this.scrollView.content.removeAllChildren();
        for(var i=0; i<this.messages.length; i++){
            var email =  this.messages[i];
            // if(!email.isShow) {
                email.isNew = email.isShow
                var node = cc.instantiate(this.emailItem);
                node.__id = email._id
                cc.find('info/title',node).getComponent(cc.Label).string = email.title;
                cc.find('info/time',node).getComponent(cc.Label).string = new Date(email.addTick).format('yyyy-MM-dd hh:mm:ss');
                cc.find('other/content/message',node).getComponent(cc.Label).string = email.content;
                // var close = cc.find('other/close',node);
                // close.show = false;
                // global.Common.addButtonHandler(this,close,'onBtnCloseEmail',email._id);
                var get = cc.find('info/get',node).getComponent(cc.Button);
                if(email.status != 2){
                    global.Common.addButtonHandler(this,get.node,'onBtnReadEmail',email._id);
                } else {
                    get.interactable = false;
                }
                
                this.scrollView.content.addChild(node);

                email.isShow = true;
            // }
        }

        this.scheduleOnce(()=>{
            var height =0;
            var news = 0;
            var children = this.scrollView.content.children
            for(var i=0; i<children.length; i++){
                if(children[i].isNew){
                    children[i].runAction(cc.fadeIn(0.2)); //等调好位置先，再慢慢显示
                    height+=children[i].height
                    news++;
                }
                children[i].isNew = false;
            }
            cc.log('height:',news,height);
            this.scrollView.scrollToOffset(cc.v2(0,height),0.01);
        },0.01)
    },

    onBtnCloseEmail(event,eid){
        for(var i=0; i<this.scrollView.content.children.length; i++){
            var node = this.scrollView.content.children[i]
            if(eid == node.__id){
                node.getChildByName('other').active = false;
            }
        }
        // event.target.parent.getChildByName('content').active = true;
    },

    onBtnReadEmail(event,eid){
        var btn = event.target
        var _node = null;
        for(var i=0; i<this.scrollView.content.children.length; i++){
            var node = this.scrollView.content.children[i]
            if(eid == node.__id){
                _node = node;
                node.getChildByName('other').active = true;
            }
        }

        this.httpGet('/game/gameUser','email.read',{e_id:eid},function(err,res){
            // var get = cc.find('info/get',_node).getComponent(cc.Button);
            // get.interactable = false;
        })

        for(var i=0; i<this.messages.length; i++){
            var email =   this.messages[i];
            if(email._id == eid){
                if(email.status == 0){
                    email.status = 1;
                    
                }
                break;
            }
        }
    },
    onPageShow(){
        this.scorllToTop()
    }
});
