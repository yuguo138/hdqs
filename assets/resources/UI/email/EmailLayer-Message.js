
cc.Class({
    extends: require('wy-Component'),

    properties: {
        scrollView:cc.ScrollView,
        messageItem :cc.Prefab,
        img:cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.messages = null;
        this.messagePage = 1;
    },

    onEnable(){
        this._super();
        // this.scrollView.node.on("bounce-top", this.scorllToTop, this);
        // this.scrollView.node.on("bounce-bottom", this.scrollToBottom, this);
    },
    onDisable(){
        // this.scrollView.node.off("bounce-top",);
    },

    scorllToTop(){
        if(this.messages != null){
            return;
        }
        var self = this;
        self.httpGet('/game/gameUser','beOperated.list',{page:this.messagePage},function(err,res){
            if(err){
                return
            }
            if(res.allRecords.length>0){
                this.img.active=false;
            }
            self.messages = res.allRecords
            // for(var i=0; i<res.allRecords.length; i++){
            //     self.messages.push(res.allRecords[i]);
            // }
            self.addMessages();
        }.bind(this))
    },
    
    updateTitleAndContent(message,node){
        var msg = cc.find('content/message',node).getComponent(cc.Label);
        if(message.opType == 12){
            msg.string = '我已经帮你把'+message.content.partId+'块菜地已经种植好'
        } else if(msg.opType == 10){
            msg.string = '我已经偷取到你的'+100+'个';
        } else if(msg.opType == 11){
            msg.string = '你的船只的'+'已经被我捣毁成功了，有种来打我啊'
        } else if(msg.opType == 13){
            msg.string = '我已经帮你把'+message.content.partId+'块菜地浇好水啦'
        }
    },

    onBtnDelete(event,_rid){
        var self = this;
        this.httpGet('/game/gameUser','beOperated.delete',{_rid:_rid},function(err,res){
            if(err){
                return;
            }
            for(var i=0; i<self.messages.length; i++){
                var message =  self.messages[i];
                if(message._rid == _rid){
                    self.messages.splice(i,1);
                    break;
                }
            }
            var children = self.scrollView.content.children
            for(var j=0; j<children.length; j++){
                if(children[i].__rid == _rid){
                    // children[i].destroy();
                    children[i].getComponent(cc.Animation).play();
                    children[i].runAction(cc.sequence(cc.delayTime(0.5),cc.removeSelf()))
                }
            }
        });
    },

    addMessages(){
        this.messagePage ++;
        this.scrollView.content.removeAllChildren();
        for(var i=0; i<this.messages.length; i++){
            var message =  this.messages[i];
            // if(!email.isShow) {
            message.isNew = message.isShow
            var node = cc.instantiate(this.messageItem);
            node.__rid = message._rid
            node.getComponent('UserInfo').setOptions(message.user);
            
            this.scrollView.content.addChild(node);
            this.updateTitleAndContent(message,node)

            message.isShow = true;
            global.Common.addButtonHandler(this,cc.find('content/delete',node),'onBtnDelete',message._rid);
            // }
        }

        

        this.scheduleOnce(()=>{
            var height =0;
            var news = 0;
            var children = this.scrollView.content.children
            for(var i=0; i<children.length; i++){
                if(children[i].isNew){
                    children[i].runAction(cc.fadeIn(0.2)); //等调好位置先，再慢慢显示
                    height+=children[i].height
                    news++;
                }
                children[i].isNew = false;
            }
            cc.log('height:',news,height);
            this.scrollView.scrollToOffset(cc.v2(0,height),0.01);
        },0.01)
    },
    onPageShow(){
        this.scorllToTop()
    }
});
