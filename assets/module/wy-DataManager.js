let data={};

module.exports={
    setData:function(key,value){
        data[key]=value;
    },
    getData:function(key){
        return data[key];
    }
}