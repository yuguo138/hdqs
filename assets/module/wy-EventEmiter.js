
//监听事件 on(eventName,callback)
//移除监听 off(eventName)

const Events =require('events');
const eventEmiter=new Events();
eventEmiter.off=eventEmiter.removeAllListeners;
module.exports=eventEmiter;