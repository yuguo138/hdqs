
cc.Class({
    extends: cc.Component,

    properties: {
        views : cc.Node,
        viewChildCount : {
            default :4,
            tooltip :"显示的节点数，只能为偶数"
        },
        scaleChild : {
            default :true,
            tooltip :"非中间节点时，是否缩放"
        },
        clickEvents: {
            "default": [],
            type: cc.Component.EventHandler,
            tooltip: "i18n:COMPONENT.button.click_events"
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if(!this.views){
            cc.warn('没有节点？');
            return;
        }
        this.blockLength = this.views.width /  this.viewChildCount
        this.views.on(cc.Node.EventType.TOUCH_START,this.onTouch,this);
        this.isMove = false;
        this.moveX = 0;
        this.localIndex = (this.viewChildCount/2)
    },

    onTouch(event){
        var point = this.views.convertToNodeSpace(event.getLocation());
        if(point.x > this.views.width/2 && !this.isMove){
            this.isMove = true;
            this._move(1);
        } else if(point.x < this.views.width/2 && !this.isMove){
            this.isMove = true;
            this._move(-1);
        }
    },

    getIndex(){
        return this.localIndex
    },

    _move(delta){
        var min = 200;
        var minObj = null;
        var max = -200;
        var maxObj = null;
        this.localIndex = (this.localIndex+this.views.children.length-delta)%this.views.children.length;
        //先找出最大位置的和最小位置和他们的节点
        for(var i=0; i<this.views.children.length; i++){
            var chd = this.views.children[i];
            if(chd.x < min) {
                min = chd.x;
            }
            if(chd.x > max){
                max = chd.x;
            }
            if(minObj == null || chd.x < minObj.x) minObj = chd;
            if(maxObj == null || chd.x > maxObj.x) maxObj = chd;
        }
        //如果最小位置马上就空出来就找最大位置的来替代它 下面相反
        if(delta == 1 && min >= -this.blockLength*2-1){
            maxObj.x = -(this.blockLength * (this.viewChildCount/2+1))
        } else if(max <= this.blockLength*2+1){
            minObj.x = (this.blockLength * (this.viewChildCount/2+1))
        }
        
        cc.Component.EventHandler.emitEvents(this.clickEvents, this,this.localIndex);

        for(var i=0; i<this.views.children.length; i++){
            var chd = this.views.children[i];
            var ex = chd.x +delta*this.blockLength
            chd.runAction(cc.moveBy(0.25,cc.v2(delta*this.blockLength,0)));
            
            if(i==0){
                chd.runAction(cc.sequence(cc.delayTime(0.3),cc.callFunc(()=>{
                    this.isMove = false
                })))
            }
            
            var scale = Math.abs(ex) < 5 ? 1 : (Math.abs(ex) <200 ? 0.8 : 0.6);

            if(this.scaleChild){
                chd.runAction(cc.scaleTo(0.25,scale))
            }
        }
    },

    setOptions(options){
        options.callback && (this.callback = options.callback);
    }
});
