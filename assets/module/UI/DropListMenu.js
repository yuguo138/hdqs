
cc.Class({
    extends: cc.Component,

    properties: {
        defaultText : '',
        defaultIndex : -1,
        text : cc.Label,
        menuIcon : cc.Node,
        menuList : {
            default : [],
            type : cc.String
        },
        selectEvents : [cc.Component.EventHandler]
    },
    onLoad(){
        if(this.text){
            this.text.string = this.defaultText;
        }
        if(this.defaultIndex >=0 && this.menuList.length > 0){
            this.setMenuList(this.menuList,this.defaultIndex);
        }
        this.node.on(cc.Node.EventType.TOUCH_END,this.onClick,this)
    },
    onClick(){
        var point = this.menuIcon.convertToWorldSpaceAR(cc.v2(this.menuIcon.width/2,this.menuIcon.height/2));

        global.EJMgr.pushUI('Eject/ListView')
        this.no_listView = global.EJMgr.getTopUI();

        // this.no_listView.getChildByName('mask').height = 360;
        for(var i=0; i<this.menuList.length; i++){
            this.no_listView.js.addItem(null,this.menuList[i]);
        }
        // var point = this.xingZuoLabel.node.convertToWorldSpace(cc.v2(45,0));
        this.no_listView.js.setOptions({
            x:point.x,
            y:point.y,
            height:360,
            callback : this.onSelectItem.bind(this)
        })
    }, 

    onSelectItem(idx){
        global.EJMgr.popUI();
        if(idx != undefined){
            this.defaultIndex = idx;
            if(this.text){
                this.text.string = this.menuList[this.defaultIndex];
            }
            this.selectCB&&this.selectCB(idx)
    
            cc.Component.EventHandler.emitEvents(this.selectEvents, this);
        }
        
    },
    getIndex(){
        return this.defaultIndex;
    },
    setSelectCallBack(cb){
        this.selectCB = cb;
    },
    setMenuList(menu,def,defText){
        this.menuList = menu
        this.defaultIndex = def
        if(this.defaultIndex >=0){
            if(this.text){
                this.text.string = this.menuList[this.defaultIndex];
            }
        } else {
            if(this.text){
                this.text.string = defText;
            }
        }
    }
});
