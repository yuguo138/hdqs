//声音播放模块
const BGSound = "resources/Audio/common/gamemusic_0.mp3";
let isPlay = false;
let BGSoundId=null;
let audioConfig={
    isBackground:true,
    isEffect:true
}
let audioConfigStr=cc.sys.localStorage.getItem("audioConfig");
if(!!audioConfigStr){
    audioConfig=JSON.parse(audioConfigStr);
}
let saveAudioConfig=function(){
    cc.sys.localStorage.setItem("audioConfig",JSON.stringify(audioConfig));
}

let Audio = {
    get isBackground(){
        return audioConfig.isBackground;
    },
    set isBackground(temp){
        audioConfig.isBackground=temp;
        saveAudioConfig();
        if(!temp){
            isPlay=false;
            cc.audioEngine.stop(BGSoundId);
            BGSoundId=null;
        }else{
            this.playBackgroundSound();
        }
    },
    set isEffect(temp){
        audioConfig.isEffect=temp;
        saveAudioConfig();
    },
    get isEffect(){
        return audioConfig.isEffect;
    },
    playBackgroundSound: function () {
        if (!isPlay&&audioConfig.isBackground) {
            BGSoundId=cc.audioEngine.play(cc.url.raw(BGSound), true, 0.1);
            isPlay = true;
        }
    },

    playEffect:function(url){
        if(audioConfig.isEffect){
            cc.audioEngine.play(cc.url.raw(url), false, 1);
        }
    },

    stopEffect:function(audioID){
        cc.audioEngine.stop(audioID);
    },

    playEffectCallback:function(url,cb){
        if(audioConfig.isEffect){
            let audioID = cc.audioEngine.play(cc.url.raw(url), false, 1);
            cc.audioEngine.setFinishCallback(audioID,cb);
            return audioID;
        }
    },
};

module.exports = Audio;