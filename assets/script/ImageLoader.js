
cc.Class({
    extends: require('wy-Component'),

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {},

    start () {

    },

    setOptions(option){
        if(option.url){
            cc.loader.load({url:option.url,type:option.type?option.type:'png'},function(err,tex){
                if(!err){
                    var sp = this.node.getComponent(cc.Sprite)
                    sp.spriteFrame = new cc.SpriteFrame(tex);
                }
            }.bind(this))
        }
    }

    // update (dt) {},
});
