var RevertType = cc.Enum({
    Text:0,
    Time:1
})

cc.Class({
    extends: cc.Component,

    properties: {
        type : {
            default : RevertType.Text,
            type : RevertType,
            tooltip:'text为正常数字\ntime为倒计时'
        },
        _title : '{0}'
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.timeLabel = this.node.getComponent(cc.Label);
        this.isRuning = false;
    },

    setTitle(t){
        this._title = t;
    },

    setTimeStart(time,end,func){
        this.end = 0;
        if(end){
            this.end = end;
        }
        this.func = func;
        this.time = time;
        this.isRuning = true;
    },

    startTimeRevert(time,func){
        this.time = time;
        this.type = RevertType.Time
        this.runTime = 0;
        this.func = func;
    },

    start(){
        this.timeLabel.string = this._title.format(Math.floor(this.time));
    },

    update (dt) {
        
        if(this.type == RevertType.Text){
            if(this.time < 0 || this.isRuning == false){
                return
            }
            var last = Math.floor(this.time);
            this.time -= dt;
            var now = Math.floor(this.time);
            if(last != now){
                this.timeLabel.string = this._title.format(now);
            }
            if(now == 0){
                this.isRuning = false;
                if(this.func){
                    this.func();
                }
            }
        } else if(this.time){
            this.runTime -= dt
            if(this.runTime < 0){
                var t = new Date().getTime();
                var delta = (this.time - t)/1000
                if(delta < 0){
                    this.time =0;
                    this.func&& this.func();

                    return;
                }
                var str = ''
                var day = Math.floor(delta / (24*3600))
                day > 0 ? str += day+'天' : ''
                var hour = Math.floor((delta % (24*3600))/3600)
                hour > 0 ? str += hour+'时' : ''
                var min = Math.floor((delta% 3600)/60)
                min > 0 ? str += min+'分' : ''
                var sec = Math.floor(delta%60)
                sec > 0 ? str += sec+'秒' : ''

                this.timeLabel.string = str
            }
            
        }
    },
});
